#include<iostream>

class Base {
	public:
		Base();
		virtual ~Base();
		void doSomething();
};

Base::Base() {
	std::cout << "Base() \n";

}
Base::~Base() {
	std::cout << "~Base() \n";
}
void Base::doSomething() {
	std::cout << "\ndoSomething() function.\n\n";
}

// Virtual Inheritance prevents doSomething function from being ambiguous.
// Without Virtual Inheritance there would be Two copies of Base class created(and destroyed twice)
// in the object creation from MultipleInheritedClass class.
class DerivedOne: public virtual Base {
	public:
		DerivedOne();
		virtual ~DerivedOne();
};

DerivedOne::DerivedOne() {
	std::cout << "DerivedOne() \n";
}

DerivedOne::~DerivedOne() {
	std::cout << "~DerivedOne() \n";
}

// Virtual Inheritance prevents doSomething function from being ambiguous.
// Without Virtual Inheritance there would be Two copies of Base class created(and destroyed twice)
// in the object creation from MultipleInheritedClass class.
class DerivedTwo: public virtual Base {
	public:
		DerivedTwo();
		virtual ~DerivedTwo();
};

DerivedTwo::DerivedTwo() {
	std::cout << "DerivedTwo() \n";
}

DerivedTwo::~DerivedTwo() {
	std::cout << "~DerivedTwo() \n";
}

class MultipleInheritedClass: public DerivedOne, public DerivedTwo {
	public:
		MultipleInheritedClass();
		~MultipleInheritedClass();
};

MultipleInheritedClass::MultipleInheritedClass() {
	std::cout << "MultipleInheritedClass() \n";
}

MultipleInheritedClass::~MultipleInheritedClass() {
	std::cout << "~MultipleInheritedClass() \n";
}

int main(int argc, char **argv) {
	//Additional scope to see Destructor calls before main ends.
	{
		MultipleInheritedClass multipleInheritedClass;
		multipleInheritedClass.doSomething();
	}

	std::cout << "\nViritual Inheritance MAIN END." << std::endl;
	return 0;
}

//@formatter:off




